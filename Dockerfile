# k-maji orquesta docs
FROM ruby:2.3

# We're creating files at the root, so we need to be root.
# USER root
ENV JEKYLL_ENV=production
ENV LC_ALL=C.UTF-8

COPY Gemfile /tmp
WORKDIR /tmp
RUN bundle install

ENV app /opt/orquesta-docs
RUN mkdir $app
WORKDIR $app
ADD . $app

RUN bundle exec jekyll build

CMD [ "bundle", "exec", "jekyll", "serve", "--force_polling", "-H", "0.0.0.0", "-P", "4000" ]
