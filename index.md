---
layout: default
title: Inicio
nav_order: 1
description: "Documentación para K-MAJI Orquesta"
permalink: /
---

# K-MAJI Orquesta

## Introducción

K-Maji Orquesta es una plataforma de orquestación de procesos de TI, está
diseñada para ayudar a los administradores de sistemas, desarrolladores de
software y otras áreas de ingenieria a simplificar los procesos de desarrollo
y operaciones de productos digitales.

En el siguiente diagrama se muestra de forma simple la arquitectura de la
plataforma:

![Arquitectura](/orquesta-docs/assets/images/arquitectura-simple.png)

La plataforma se basa en los principios de *Infrastructure as Code*, en donde
todo el equipo almacena en un repositorio central el código y la documentación
de la infraestructura, y se mantienen flujos para el control y revisión de
cambios, su liberación y despliegue a los diferentes ambientes de ejecución.

Probamos y seleccionamos solo las mejores herramientas para automatizar las
diferentes fases de procesos como CI (*Continouous Integration*) y CD
(*Continouous Delivery*) usando software libre como Jenkins, Packer, Ansible,
Inspec, Selenium y otras herramientas que hemos integrado para automatizar la
estandarización de configuraciones de los servicios estandar en servidores
Linux, desplegar aplicaciones web con todo el stack de servidores web,
servidores de aplicaciones, bases de datos, configuraciones y pruebas iniciales.

En este documento se describen los requisitos de hardware y software necesarios
en la implementación del *K-MAJI Orquesta* en un ambiente de pruebas.

### Objetivos

En esta lista describimos los objetivos que queremos lograr con esta guía:

* Presentar la arquitectura de la plataforma.
* Precisar los requisitos de hardware y de software.
* Establecer los requisitos de comunicación entre los diferentes componentes.
* Puntualizar los capacidades esperadas del personal asignado a la
implementación del sistema.
* Dar a conocer los requisitos de los controles de acceso entre los diferentes
componentes.
* Detallar el procedimiento de despliegue inicial de la plataforma y sus
componentes.
* Describir los procesos de despliegue de herramientas como iTop para gestión
de una CMDB.
