# Documentación K-MAJI Orquesta

**IMPORTANT:** Este repositorio ha sido archivado por falta de mantenimiento en los últimos años.

Este repositorio almacena el código de la guía de implementación de K-MAJI
Orquesta.

Si deseas contrubuir revisando y escribiendo documentación, necesitas instalar
*docker* y *make* en tu equipo personal para automatizar la construcción en
línea.

Una vez que has hecho cambios en el código de la documentación, debes construir
la documentación en línea de forma local antes de publicarla.

```
$ make build; make test
```

Abre un navegador en
[http://localhost:4000/orquesta-docs](http://localhost:4000/orquesta-docs)
y prueba con tus propios ojos.

Haga clic en [documentación](https://kronops.gitlab.io/orquesta-docs) para
ver la versión en línea de la guía de K-MJI Orquesta.
