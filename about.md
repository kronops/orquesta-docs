---
layout: page
title: Acerca de nosotros
permalink: /about/
nav_order: 999
---

# [KronOps](https://kronops.com.mx)

Somos un equipo de gente apasionada cuya meta es mejorar la vida de cada uno a través de productos disruptivos. Construimos grandes productos para solucionar sus problemas de negocio.

Nuestros productos están diseñados para pequeñas o medianas empresas que quieran optimizar su rendimiento.

[Conozca más de nosotros](https://kronops.com.mx/nosotros).

Acceda a la página del proyecto de K-MAJI Orquesta para reportar bugs y hacer sugerencias en [Gitlab](https://gitlab.com/kronops/k-maji-orquesta).
