# K-MAJI Orquesta GitLab Flow

## Introducción

En este documento describimos la implementación de un flujo de trabajo Git para gestionar las versiones del código de K-MAJI Orquesta, con esto experimentaremos como tener un flujo más eficiente que nos permita mejores liberaciones y despliegues.

### Objetivos

 * Describir las necesidades de una estrategia de branching.
 * Describir porque necesitamos implementar un flujo git.
 * Describir porque necesitamos implementar revisión de código.
 * Describir los beneficios de gitlab flow.

## Requisitos

Este flujo debe de:

 * Definir estrategía de branching
 * Implementar revisión de código
 * Implementar merge requests.

## La estrategia de branching

Seguiremos la guía de gitlab flow, usaremos las ramas:

 * master
 * feature-XXX
 * production
 * hotfix

Las ramas feature-XXX se hacen merge a master, de master se hace branch production, de production salen hotfix y se hacen merge a production y luego se hace merge a master.

## La revisión de código

Además de usar gitlab flow como estrategía de branching, se debe implementar la revisión de código para mejorar la calidad de las liberaciones, requisitos:

 * Revisión en pares.
 * Usando votación.

## Merge Requests

Se debe implementar un proceso de validación para hacer el merge a master desde un feature branch, antes requiere revisión de código para el merge request y la votación.

Ya estamos probando revisión de código con merge requestas, ya hemos hecho configuraciones en gitlab para proteger la rama maestra solo al dueño del repositorio.

Ya hicimos el merge a master de un par de feature branches, hasta ahora se ha hecho correctamente sin conflictos, seguiremos probando un par de merge más antes de liberar el proceso para producción.

## Referencias

En esta sección hacemos referencia a documentación que usamos de ejemplo para construir implementar gitlab flow en el sistema de Orquestación de TI.

Git:
https://git-scm.com

GitLab:
https://gitlab.com

Git Flow:
https://danielkummer.github.io/git-flow-cheatsheet/

GitLab Flow:
https://about.gitlab.com/2014/09/29/gitlab-flow/

The 11 Rules of GitLab Flow:
https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/

GitLab Flow Solo:
https://es.slideshare.net/viniciusban/gitlab-flow-solo-39625228

GitLab Flow presentation:
https://speakerdeck.com/ogom/gitlab-flow

Demo: Mastering code review with GitLab:
https://about.gitlab.com/2017/03/17/demo-mastering-code-review-with-gitlab/

Code Review Guidelines:
https://docs.gitlab.com/ee/development/code_review.html

Code Review Via GitLab Merge Requests:
https://yalantis.com/blog/code-review-via-gitlab-merge-requests-code-review-must/
