---
layout: page
title: Monitoreo de Trabajos
parent: Portal de Operaciones
nav_order: 3
---

# Monitoreo de trabajos

Al acceder a los detalles de una tarea (no de una ejecución, las cuales se
diferencian por presentar un numeral # y el id de ejecución) se encontrará con
una página como esta:

![Detalles de tarea](/orquesta-docs/assets/images/detalles-job.png)

Cuando una ejecución está en proceso, mostrará un icono estilo “loading”. Si ha
finalizado, puede tener un signo de éxito o fallo. En cualquier caso, al dar
click al icono del estado de ejecución será transferido a la página que le
muestra lo que ocurre en el shell durante la ejecución de la tarea, la **salida
de consola**.

![Consola desde estado de ejecución](/orquesta-docs/assets/images/console-shortcut.png)

Esta le puede servir como un log si la tarea ya finalizó, o monitorear los
pasos que realiza la ejecución del despliegue, y en caso de que exista algún
fallo le ayudará a identificar el error.

![Salida de Consola](/orquesta-docs/assets/images/salida-consola.png)

**NOTA**: El método anterior es un atajo, también puede ver la salida de
consola ingresando a la página de detalles de una ejecución (las del numeral
mencionado arriba) y seleccionar **Console Output** en la opciones de esa
ejecución.

![Desde detalle de ejecución](/orquesta-docs/assets/images/detalle-ejecucion.png)
