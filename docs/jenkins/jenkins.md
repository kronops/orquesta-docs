---
layout: default
title: Portal de Operaciones
permalink: /docs/jenkins/
has_children: true
nav_order: 8
---

# Portal de Operaciones

Ahora que está desplegado el servicio Jenkins y establecidas todas las
conexiones con los demas nodos que ya existen en el inventario, es momento de
utilizar K-MAJI Orquesta para comenzar a automatizar su infraestructura. Para
tomar control del orquestador, deberá familiarizarse con el uso del Portal de
Operaciones.
