---
layout: page
title: Etapas del Pipeline
parent: Ejecución de Pipelines
grand_parent: Portal de Operaciones
nav_order: 4
---

# Etapas del Pipeline

Un pipeline se lleva a cabo en **etapas**. Es posible ver el avance individual
de cada una de manera gráfica. En la parte principal de la página podrá
observar varias **cajas** que representan el avance de cada etapa.

![Etapas](/orquesta-docs/assets/images/etapas.png)

## Detalles por Etapa

En los detalles de un pipeline, al posicionar el puntero sobre la caja de
avance de cualquier etapa, se despliega un botón de acceso a los **Logs**. Es
posible acceder a los detalles dando click en  el botón **Logs**, el cual
abrirá una tarjeta con vínculos a las tareas que pertenecen a esa etapa.

![Detalles etapa](/orquesta-docs/assets/images/detalles-etapa.png)

Algunas etapas involucran más de una tarea. Deberá expandir las opciones de la
tarea para ir a la respectiva página de detalles de tarea (general) o de
ejecución (particular).

![Jobs de etapa](/orquesta-docs/assets/images/etapa-jobs.png)

**Scheduling project** abrirá la página de detalles de la tarea (semejante a
los detalles de un pipeline) donde podrá ver el **historial de ejecuciones del
trabajo**. **Starting building** es un acceso a los detalles de la  ejecución
que generó el pipeline. Esto es útil al momento de conseguir información sobre
algún error en los despliegues.

![Jobs Expandidos](/orquesta-docs/assets/images/jobs-expandidos.png)
