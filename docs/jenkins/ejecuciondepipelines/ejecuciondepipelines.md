---
layout: default
title: Ejecución de Pipelines
parent: Portal de Operaciones
permalink: /docs/jenkins/ejecuciones/
has_children: true
nav_order: 3
---
# Ejecucion de Pipelines


Al acceder como administrador en el portal Jenkins, se muestra la lista de
todos los trabajos disponibles bajo la vista (o pestaña) **Todo**. En la vista
**IT-Deploys** encontrará los pipelines del despliegue de servicios.

![IT-Deploys](/orquesta-docs/assets/images/it-deploys.png)

Los pipelines configurarán automáticamente los componentes de K-MAJI Orquesta.
Esta es la secuencia en que se deben ejecutar los pipelines:

1. Deployer >> PPL_Deploy_Orquesta
2. CMDB >> PPL_Deploy_Cmdb
3. Monitor >> PPL_Deploy_Monitor

Al finalizar las ejecuciones, la infraestructura se encuentra ya operativa y
configurada con un ambiente inicial. En las siguientes secciones se describen
los métodos para disparar el despliegue de los Pipelines
