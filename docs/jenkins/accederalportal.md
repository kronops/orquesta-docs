---
layout: page
title: Acceder al Portal
parent: Portal de Operaciones
nav_order: 2
---

# Acceder al Portal del servidor Jenkins

Con un navegador ingrese al puerto 8080 de la máquina deployer. Para la
instancia representada en esta guía, el acceso se encontrará en la siguiente
dirección: ```http://192.168.1.10:8080```.

![Landing](/orquesta-docs/assets/images/landing-jenkins.png)

Para ejecutar cualquier tarea del orquestador, deberá iniciar sesión como
administrador en el portal Jenkins. En la cabecera del portal, de clic a
**INICIAR SESIÓN** en la esquina superior derecha.

![Login](/orquesta-docs/assets/images/login-jenkins.png)

Si se encuentra en un equipo seguro, puede seleccionar **Keep me signed in**
para mantener la sesión activa.
