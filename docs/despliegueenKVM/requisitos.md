---
layout: page
title: Requisitos
parent: Despliegue en Máquinas Virtuales KVM
nav_order: 2
---

# Requisitos

Para instalar K-MAJI Orquesta vamos a necesitar un pool de servidores Linux con
el sistema operativo Ubuntu 16.04, cada máquina deberá estar conectada a una
misma red, en donde se tenga comunicación SSH (TCP/22) entre los diferentes
servidores. Todos ellos deberán tener el mismo usuario, y el mismo password de
ese usuario (tanto para login como para sudo). Este requisito es indispensable
para el despliegue inicial, y los passwords deben cambiarse una vez que la
instancia de K-MAJI Orquesta esté operativa.

Se necesitan al menos 4 servidores que albergarán los elementos del orquestador.
Tener a la mano sus direcciones IP dentro de su ambiente le brindará una
instalación más ágil. Para fines de esta guía, se usarán las siguiente máquinas:

| **Servicio** | **IP** |
|----------|----|
| Deployer | 192.168.1.10
| Tester | 192.168.1.11 |
| CMDB | 192.168.1.12 |
| Monitor | 192.168.1.13 |

**IMPORTANTE:** Las IPs usadas son para fines demostrativos.
