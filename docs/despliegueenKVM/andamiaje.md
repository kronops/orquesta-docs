---
layout: page
title: Andamiaje
parent: Despliegue en Máquinas Virtuales KVM
nav_order: 4
---

# Andamiaje

Para comenzar con la instalación de K-MAJI Orquesta, cada nodo de la  
infraestructura (Deployer, Tester, CMDB y Monitor) deberá ejecutar con permisos
de sudo el script que instala las dependencias necesarias.

```
sysadmin@host:~$ wget https://gitlab.com/kronops/k-maji-orquesta/raw/master/bin/bootstrap-node.sh
sysadmin@host:~$ sudo bash bootstrap-node.sh
```

Una vez que las dependencias estén cubiertas en cada máquina (nodo) del pool de
servidores y el uso de proxy esté correctamente configurado,  deberá preparar el
script de instalación.
