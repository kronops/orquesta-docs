---
layout: page
title: Comunicaciones
parent: Requerimientos
nav_order: 4
---

# Comunicaciones

Para el ambiente de pruebas se recomienda que las máquinas para los servicios
de orquestación y las demás máquinas que se administrarán se encuentren en una
misma red privada virtual, puede ser un VPC de AWS ó un bridge de KVM/Libvirt.
También se recomienda que las máquinas tengan correctamente configurado el
direccionamiento IPv4 en forma estática de acuerdo a la tabla de
requerimientos. Se recomienda que todas estén en la misma subred IP.

Para la comunicación entre sistemas, tanto para las interfaces usuario-servidor
como para las de servidor-servidor, se deben autorizar los siguientes
protocolos de comunicación:

- Los **Usuarios Desarrolladores** de infra deberán tener acceso desde sus
estaciones de trabajo al **Servidor Gitlab (SCM)** y al **Servidor Deployer**
vía SSH y HTTPS.
- **SCM Master - Deployer:** Se deben permitir las comunicaciones para que
desde el servidor Deployer haya conexión con el servidor que administra el
código de la infraestructura. El protocolo es GIT, sobre SSH TCP/22 y HTTPS
TCP/443.
- **CMDB - Deployer:**  El protocolo es HTTP TCP/80 y TCP/443..
- **Deployer - Nodos:** Se debe habilitar la comunicación entre el servidor
Jenkins y el servidor cluster manager. El protocolo es SSH TCP/22.
- **Nodos orquesta - Internet:** Se debe proveer de acceso vía default gateway
a Internet o en su defecto a través de un proxy web.

Adicionalmente, se recomienda que todos los servidores tengan asociado un
nombre DNS en la red privada, así como tener configurada la resolución reversa
en el DNS de la red privada. Esto para volver más eficientes las comunicaciones
en cuanto a tiempos de negociación y tener mayor control en la autorización
para los diferentes servicios.

Para aquellos servidores que proveen servicios de forma segura, es decir,
usando certificados SSL/TLS, se deberá proveer un certificado oficial o uno
creado por una entidad certificadora privada.
