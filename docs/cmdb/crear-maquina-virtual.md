---
layout: page
title: Crear Maquina Virtual
parent: Administracion de CMDB
nav_order: 7
---

# Dar de Alta una Máquina Virtual

Teniendo ya un hypervisor en el inventario, es posible registrar la máquina
virtual. En la sección Virtualización  haga clic en **Crear Maquina Virtual.**

![Creación de una Maquina Virtual](/orquesta-docs/assets/images/crear-maquina-virtual.png)

Ahora deberá capturar los datos del servidor (máquina virtual).

![Creación de una Maquina Virtual](/orquesta-docs/assets/images/creacion-de-maquina-virtual.png)

Como se puede observar, cada tipo de objeto tiene diferentes atributos y
relaciones, algunos con más relevancia que otros.

Para las máquinas virtuales, los campos obligatorios a llenar son:
  - **Nombre:** Debe ser igual al nombre del host (hostname) del servidor o
  máquina virtual (se recomienda que sea el nombre corto, sin el domain name).
  Este debe poder ser resuelto por el servidor DNS.
  - **Organización:** El nombre de la organización a la que está asociado el
  servidor. Si se ha seguido la instrucción de configurar su organización en
  la esquina superior izquierda, la selección será automática.
  - **Estatus:** El estado debe ser no productivo o productivo.
  - Criticidad para el Negocio: El nivel debe ser definido por el usuario (por
  default Media).
  - **Host Virtual:** El nombre del servidor hypervisor, previamente creado, y
  debe estar asociado a una granja de virtualización.

Campos opcionales:
  - **Familia de SO:** Definir como Linux.
  - **Versión de SO:** Seleccionar Ubuntu Server 16.04.
  - **CPU:** El número de vCPUs.
  - **RAM:** Indicar la cantidad en Mb.

Después de definir los datos en las propiedades generales, deberá ir a la
pestaña **Interfaces de Red** para asociar a la máquina virtual la tarjeta de
red y su dirección MAC:

![Creación de una Maquina Virtual](/orquesta-docs/assets/images/interfases-de-red.png)

Al hacer clic sobre el botón **Crear Interfaz Lógica**, aparecerá una ventana
como la siguiente:

![Creación de una Maquina Virtual](/orquesta-docs/assets/images/creacion-de-interfaz-logica.png)

Los campos a llenar son:
  - **Nombre:** Se recomienda poner **eth0** para la primer interfaz.
  - **Dirección MAC:** Se debe escribir en minúsculas y con “:” como separador,
  por ejemplo: **52:54:00:c1:a1:1c.**
  - **Velocidad:** especificar la cantidad en Mbps, por ejemplo **100** o
  **1000.**

A continuación deberá ir a la pestaña **Contactos**, y después hacer clic en
el botón **Agregar Contacto:**

![Creación de una Maquina Virtual](/orquesta-docs/assets/images/contactos-para-este-ec.png)

Le aparecerá una lista con los contactos agregados en el sistema para la
organización seleccionada. Elija el o los contactos y haga clic en **Agregar:**

![Creación de una Maquina Virtual](/orquesta-docs/assets/images/agregar-relacion-contacto.png)

Por último, haga clic en el botón **Crear** para dar de alta el servidor.

![Creación de una Maquina Virtual](/orquesta-docs/assets/images/boton-crear-maquina-virtual.png)

Para cerciorarse de que el equipo se agregó correctamente, es aconsejable hacer
una búsqueda general en el campo del lado superior derecho del portal iTop:

![Creación de una Maquina Virtual](/orquesta-docs/assets/images/busqueda-maquina-virtual.png)

En el resultado se pueden observar las entradas del inventario de iTop
relacionadas con la búsqueda. Además de la máquina virtual, es normal ver otros
objetos asociados al nombre de la máquina, tales como la interfaz lógica o
tickets (cuando existan).

![Creación de una Maquina Virtual](/orquesta-docs/assets/images/resultados-para-deployer.png)
