---
layout: default
title: Administracion de CMDB
permalink: /docs/cmdb/
has_children: true
nav_order: 9
---

# Introducción
En esta sección se muestra cómo ejecutar los procesos de alta, baja y
modificación de registros de máquinas virtuales en el inventario de la CMDB
iTop. Estos deberán ser ejecutados por los operadores o administradores de
sistema para mantener un catálogo de servicios y servidores actualizado.

## Objetivos

Homologar el proceso de alta, baja y modificación de los registros de máquinas
en el inventario central para mejorar el control de cambios en servidores.
