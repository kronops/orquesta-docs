
```shell
$ cd ~/data/vcs
$ git clone https://gitlab.com/kronops/k-maji-orquesta.git
$ cd k-maji-orquesta
```

Ahora debemos definir los siguientes parámetros de construcción:

 * Nombre de proyecto
 * Nombre de ambiente
 * Nombre de dominio
 * SSH user

El nombre de proyecto se puede asociar facilmente a algún producto o quizas un cliente, el nombre de ambiente puede ser cualquier de los diferentes estadios por los que pasa el desarrollo de un producto, como puede ser 'development', 'testing' y 'production', y finalmente el nombre del dominio debe ser el nombre DNS, puede ser un dominio privado o uno público. El usuario SSH user es el usuario que usará para conectarse a las máquinas virtuales que creo con vagrant u otro sistema de virtualización o cloud.

Para establecer estos parámetros se deben definir como variables de ambiente:

 * *PROJECT_NAME*
 * *PROJECT_ENV*
 * *PROJECT_DOMAIN*
 * *SSH_TMP_USER*

Estas variables de ambiente y sus valores se escriben en el archivo *.env* en la raíz del proyecto.

El archivo $(PWD)/.env no existe por defecto, se debe crear, por ejemplo:

```shell
$ vim .env
# Build definitions
PROJECT_NAME=local
PROJECT_ENV=development
PROJECT_DOMAIN=example.com
SSH_TMP_USER=vagrant
export PROJECT_NAME
export PROJECT_ENV
export PROJECT_DOMAIN
export SSH_TMP_USER
```

Esta configuración necesita que se cree un directorio del proyecto, y un archivo de inventario para el ambiente, esto se debe hacer en el directorio *inventory*, por ejemplo:

```shell
$ source .env
$ mkdir -p inventory/$PROJECT_NAME
```

También se deben generar llaves SSH que se usarán para conectarse a los nodos a administrar.


Ahora generamos las llaves SSH para Ansible y Jenkins:

```shell
$ bin/generate-ssh-keys.sh
```

El resultado de el script anterior es un par de llaves SSH RSA de 4096 bytes para ansible y para jenkins en el directorio /etc/ansible/inventory/$PROJECT_NAME/.ssh, por ejemplo:

```shell
$ ls -l inventory/local/.ssh/
total 16
-rw------- 1 jmedina jmedina 3243 Aug 21 18:31 id-local-ansible.rsa
-rw------- 1 jmedina jmedina  745 Aug 21 18:31 id-local-ansible.rsa.pub
-rw------- 1 jmedina jmedina 3247 Aug 21 18:31 id-local-jenkins.rsa
-rw------- 1 jmedina jmedina  745 Aug 21 18:31 id-local-jenkins.rsa.pub
```

Y por último creamos el archivo del inventario.

```shell
$ vim inventory/$PROJECT_NAME/$PROJECT_ENV
########################################
#####   ANSIBLE INVENTORY          #####
#### (MARK: LOCAL: ENV: DEVELOPMENT) ###
########################################

#
# Global Variables
#
[all:vars]
dns_public_domain=example.com
dns_private_domain=example.com
authorized_keys_for_central_servers=/etc/ansible/inventory/local/.ssh/id-local-ansible.rsa.pub
#ansible_ssh_port=22
#ansible_ssh_user=root
#ansible_ssh_private_key_file=/etc/ansible/inventory/local/.ssh/id-local-ansible.rsa
# Jenkins slaves (agentes Swarm)
jenkins_url=http://192.168.33.10:8080
#
# Central Servers
#

[central:children]
central-devops
central-cms
central_tester

[central:vars]
timezone=America/Mexico_City
# This user has unlimited sudo privileges, it is created at install time.
useradmin_name=vagrant

[central-devops]
devops.example.com  ansible_ssh_host=192.168.33.10

[central-cms]
cms.example.com  ansible_ssh_host=192.168.33.4

[central_tester]
tester.example.com  ansible_ssh_host=192.168.33.5

```

**IMPORTANTE:** Asegurese de cambiar la ruta de las variables ansible_ssh_private_key_file hacía la ruta de la llave privada que recien generamos.

**IMPORTANTE:** Asegurese de cambiar la variable authorized_keys_for_central_servers, esta debe apuntar a la ruta del archivo con la llave pública que se despliega a los servidores a administrar, por ejemplo: inventory/local/.ssh/id-local-ansible.rsa.pub.

## Construcción

Para construir un ambiente de desarrollo local para el sistema de orquestación de TI usaremos el comando make y las configuraciones definidas en el archivo Makefile.

Ejecutaremos el subproceso vagrant para crear un ambiente de desarrollo local basado en Vagrant y Virtualbox, crearemos tres máquinas virtuales basadas en la distribución GNU/Linux Ubuntu server 16.04 de 64-bits, una máquina se llama *devops* y será donde construiremos el sistema de orequestación, otra llamada *tester* desde donde automatizaremos pruebas de infraestructura y web y la otra se llama *cms* que es donde desplegaremos el servicio Wordpress.

Ejecuta el siguiente procedimiento dentro del directorio en el cual descargaste el repositorio de git, por ejemplo:

```shell
$ make vagrant
```

Una vez que las máquinas virtuales se hayan creado, continuamos el proceso de construcción del sistema de orquestación.

Lo primero que debes hacer es conectarte a la máquina devops, por ejemplo:

```shell
$ vagrant ssh devops


vagrant@devops:~$
```

Una vez que estés dentro de la máquina virtual devops, escala privilegios a la cuenta root y cambiate al directorio */vagrant*, por ejemplo:

```shell
vagrant@devops:~$ sudo -i

root@devops:~# cd /vagrant

```

Continuaremos usando make para ejecutar los diferentes subprocesos que nos permitirán consturir el sistema de orquestación de TI.

Primero debemos hacer el target *bootstrap*, el cual inicializa el sistema, es decir, instalaremos ansible y todos su requisitos para empezar a construir el sistema de orquestación.

```shell
root@devops:/vagrant# make bootstrap
```
```shell
root@devops:/vagrant# make build
```

Si el subproceso de build para el entorno de desarrollo local termina exitosamente, ejecuta el target *test* para validar la sintaxis de los playbooks de Ansible.

```shell
root@devops:/vagrant# make test
```

## Despliegue

Antes de poder desplegar algún playbook a las máquinas virtuales, se deben desplegar las llaves ssh a los diferentes servidores, para eso, ejecute el siguiente script:


```shell
root@devops:/vagrant# bin/deploy-ansible-keys.sh
```

**IMPORTANTE:** El script pregunta la contraseña del usuario vagrant, luego vuelve a preguntarla para elevar privilegios con sudo.

Ahora debes volver a modificar el archivo del inventario que has creado en inventory/local/development, modifique las variables globales en '[all:vars]', por ejemplo:

```shell
root@devops:/vagrant# vim inventory/local/development
...
...
...
[all:vars]
...
...
...
ansible_ssh_port=22
ansible_ssh_user=root
ansible_ssh_private_key_file=/etc/ansible/inventory/local/.ssh/id-local-ansible.rsa
```

Después de haber construido el ambiente de desarrollo local para el sistema de orquestación, lo deployamos en la máquina virtual devops usando el parámetro *deploy*, por ejemplo:

```shell
root@devops:/vagrant# make deploy
```

Una vez que termine el proceso de deploy, ingresa a la consola de Jenkins desde el navegador en tu equipo local apuntando al siguiente URL: http://localhost:1088.

**IMPORTANTE:** El puerto *1088* está definido en la configuración de vagrant, este puerto se abre en el localhost y es re enviado al puerto *8080* de la maquina devops.

Las credenciales de acceso a Jenkins predefinidas son:

 - Usuario: admin
 - Contraseña: admin


Para conocer más acerca del uso de Jenkins sigue con la guía operación del sistema de orquestación de TI.

Para desplegar el servicio wordpress, se puede hacer directamente ejecutando el playbook de ansible, por ejemplo:

```shell
root@devops:/vagrant# cd /etc/ansible
root@devops:/etc/ansible# ansible-playbook central-cms-servers.yml
```
Una vez que termine el proceso de deploy wordpress, ingresa al asistente de configuración de wordpress desde el navegador en tu equipo local apuntando al siguiente URL: http://localhost:1080.

**IMPORTANTE:** El puerto *1080* está definido en la configuración de vagrant, este puerto se abre en el localhost y es re enviado al puerto *80* de la maquina cms.  


## Limpieza

Si realizaste cambios en la configuración de ansible y su inventario local y deseas revertir los cambios ejecuta make con el parámetro *clean*, por ejemplo:

```shell
root@deployer:/vagrant# make clean
```

**IMPORTANTE:** Este comando debe ejecutarse en la máquina virtual deployer.

## Destrucción

Para destruir el ambiente de desarrollo local que creaste, debes ejecutar el comando make con el parámetro *destroy*, por ejemplo:

```shell
$ make destroy
```

**IMPORTANTE:** Este comando debe ejecutarse en tu equipo local, en donde corre vagrant.

## Personalización

Cuando se construye la plataforma de orquestación, esta ya incluye algunas configuraciones parametrizadas para un ambiente de desarrollo local, sin embargo, se puede usar como base para desplegar a otros ambientes, tales como pruebas o incluso puede desplegar algunos roles a producción.

Los parámetros principales con los que se construye la plataforma son:

 * Proyecto: local
 * Ambiente: development
 * Dominio DNS: example.com
 * Servidor GIT: gitlab.com

**NOTA:** Estos parámetros los toma de el archivo '~/.env como se menciona en secciones atras.

Algunos de estos parámetros están definidos en el archivo del inventario, es decir, en */etc/ansible/inventory/local/development*.

Esta ruta hacía el inventario se define en el archivo */etc/ansible/ansible.cfg*.

La llave SSH con la que ansible se conecta a los servidores para realizar las tareas de automatización es una llave generica, si va a implementar este sistema en un ambiente de pruebas o producción debe asegurarse de cambiar la llave, la configuración actual está así en el archivo de inventario:

```
...
...
...

[central_deployer:vars]
ansible_ssh_port=22
ansible_ssh_user=root
ansible_ssh_private_key_file=/etc/ansible/inventory/local/.ssh/id-local-ansible.rsa

...
...
...

[central-cms:vars]
ansible_ssh_port=22
ansible_ssh_user=root
ansible_ssh_private_key_file=/etc/ansible/inventory/local/.ssh/id-local-ansible.rsa

```
Como se puede ver, las variables de conexión para los grupos de servidores central_deployer y central-cms usan la llave definida por el parámetro *ansible_ssh_private_key_file*, en este caso la llave se almacena en el subdirectorio *.ssh* junto con el inventario de ansible.

Es recomendado que generes tu propia llave SSH y la parametrices en el inventario.

Para personalizar la contrucción del proyecto desde jenkins se deben definir en el archivo .env que se crea al inicio, estas son importadas por  el job 001-BUILD_INFRA las variables 'PROJECT_NAME', 'PROJECT_ENV' y 'PROJECT_DOMAIN'.

Para generar nuevas llaves, se debe usar el script bin/generate-ssh-keys.sh, este script lee las variables de .env y genera dos llaves, una para usarla con ansible y otra de jenkins.

El script se ejecuta así:

```
$ bin/generate-ssh-keys.sh
```

Este scrip generará los archivos de las llaves en el directorio /etc/ansible/inventory/$PROJECT_NAME/.ssh.


## Mejores prácticas

Estos playbooks se deben alinear a la estructura actual de ansible que es proyecto:clase servidor:tipo servidor: centro servidor.

Para construir la estructura del rol se debe usar ansible-galaxy, el nombre debe ser rol-service.

**IMPORTANTE:** Los roles deben usar guión medio para separar las palabras que componen el nombre del rol, se aconseja que los nombres de los roles terminen con "-service".

Se deben seguir las mejores prácticas de edición en relación a :

 * Escribir nombres de tareas descriptivas y en inglés.
 * Identado basado en espacios en blanco.
 * Lineas en blanco para separar cada tarea
 * Uso de variables en formato ansible 2.x, ejemplo ansible_ssh_host en lugar de ansible_host.
 * Uso de variables especificas para el componente en vars/main.yml.
 * Uso de variables especificas para el ambiente en defaults/main.yml.
 * Uso de variables especificas para el centro en inventory/${PROJECT_NAME}/production.
 * Uso de variables estandar en base a el inventario o facts para evitar re escribir funciones de shell para capturar datos.
 * Se deben usar las funciones de python como split para sacar variables personalizadas siempre en base a variables exitentes en inventario, rol o fact.
 * Se deben usar tags para cada tarea, si son generales que se use la etiqueta general rol_service, note el guion bajo, no se usan guión medio "-".

Una vez desarrollado el playbook del rol, se debe agregar el rol al playbook de cada tipo de servidor abajo del rol de snmp-service.

Los cambios deben integrarse al repositorio git de acuerdo al flujo gitlab y realizar un merge request para que se valide.
